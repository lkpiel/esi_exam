package com.example.common.rest.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.math.BigDecimal;

/**
 * Created by lkpiel on 6/9/2017.
 */
@Data
@Getter
@EqualsAndHashCode
public class TransactionDTO {
    String bookingOrder;
    String transactionURL;
    BigDecimal amount;
}
