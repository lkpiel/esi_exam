package com.example.booking.application.services;

import com.example.common.rest.ExtendedLink;
import com.example.user.application.services.UserAssembler;
import com.example.booking.application.dto.BookingOrderDTO;
import com.example.booking.domain.model.BookingOrder;
import com.example.booking.rest.controllers.BookingRestController;
import com.example.property.application.services.PropertyAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.http.HttpMethod.POST;

/**
 * Created by lkpiel on 5/24/2017.
 */
@Service
public class BookingOrderAssembler extends ResourceAssemblerSupport<BookingOrder, BookingOrderDTO> {
    @Autowired
    UserAssembler userAssembler;
    @Autowired
    PropertyAssembler propertyAssembler;
    public BookingOrderAssembler() {
        super(BookingRestController.class, BookingOrderDTO.class);
    }

    @Override
    public BookingOrderDTO toResource(BookingOrder booking) {
        if (booking != null) {
            BookingOrderDTO dto = createResourceWithId(booking.getId(), booking);
            dto.set_id(booking.getId());
            dto.setStartDate(booking.getStartDate());
            dto.setEndDate(booking.getEndDate());
            dto.setStatus(booking.getStatus());
            dto.setTransaction(booking.getTransaction());
            dto.setUser(userAssembler.toResource(booking.getUser()));
            dto.setProperty(propertyAssembler.toResource(booking.getProperty()));
            dto.add(new ExtendedLink(
                    linkTo(methodOn(BookingRestController.class)
                            .cancelBooking(dto.get_id())).toString(),
                    "cancel", POST));
            return dto;
        }
        return null;
    }
    public BookingOrder toEntity(BookingOrderDTO bookingOrderDTO) {
        if(bookingOrderDTO != null) {
            BookingOrder booking = BookingOrder.of(bookingOrderDTO.get_id(),bookingOrderDTO.getStartDate(),bookingOrderDTO.getEndDate(),bookingOrderDTO.getTransaction(),userAssembler.toEntity(bookingOrderDTO.getUser()), propertyAssembler.toEntity(bookingOrderDTO.getProperty()),bookingOrderDTO.getStatus());
            return booking;
        }
        return null;
    }
    public List<BookingOrderDTO> toResources(List<BookingOrder> bookings) {
        if(bookings.size() < 1){
            return new ArrayList<BookingOrderDTO>();
        }
        return bookings.stream().map(p -> toResource(p)).collect(Collectors.toList());
    }
    public List<BookingOrder> toEntities(List<BookingOrderDTO> bookingOrderDTOS) {
        if(bookingOrderDTOS.size() < 1){
            return new ArrayList<BookingOrder>();
        }
        return bookingOrderDTOS.stream().map(p -> toEntity(p)).collect(Collectors.toList());
    }


}
