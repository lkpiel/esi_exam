package com.example.booking.application.dto;

import com.example.booking.domain.model.BookingOrderStatus;
import com.example.property.application.dto.PropertyDTO;
import com.example.user.application.dto.UserDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDate;

/**
 * Created by lkpiel on 5/24/2017.
 */
@Data
@Getter
@EqualsAndHashCode
public class BookingOrderDTO extends com.example.common.rest.ResourceSupport {
    String _id;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    LocalDate startDate;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    LocalDate endDate;

    String transaction;

    PropertyDTO property;
    UserDTO user;

    @Enumerated(EnumType.STRING)
    BookingOrderStatus status;
}
