package com.example.booking.application.services;

import com.example.booking.application.dto.BookingOrderDTO;
import com.example.booking.domain.model.BookingOrder;
import com.example.booking.domain.model.BookingOrderStatus;
import com.example.booking.domain.repository.BookingOrderRepository;
import com.example.booking.infrastructure.BookingIdentifierFactory;
import com.example.property.application.dto.PropertyDTO;
import com.example.property.application.services.PropertyAssembler;
import com.example.property.application.services.PropertyService;
import com.example.user.application.dto.UserDTO;
import com.example.user.application.services.UserAssembler;
import com.example.user.domain.model.User;
import com.example.user.domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

/**
 * Created by lkpiel on 5/24/2017.
 */
@Service
public class BookingService {
    @Autowired
    BookingOrderRepository bookingOrderRepository;
    @Autowired
    BookingOrderAssembler bookingOrderAssembler;
    @Autowired
    BookingIdentifierFactory bookingIdentifierFactory;
    @Autowired
    PropertyService propertyService;
    @Autowired
    PropertyAssembler propertyAssembler;
    @Autowired
    UserAssembler userAssembler;
    @Autowired
    UserRepository userRepository;
    public BookingOrderDTO createBooking(BookingOrderDTO bookingOrderDTO) {
        UserDTO booker = bookingOrderDTO.getUser();
        List<BookingOrderDTO> orders =  booker.getBookings();
        BigDecimal reservedBalance = BigDecimal.ZERO;
        for(BookingOrderDTO order: orders){
            if(order.getStatus().equals(BookingOrderStatus.ACCEPTED))
                reservedBalance.add(order.getProperty().getPrice());
        }
        if(booker.getBalance().subtract(reservedBalance).divide(BigDecimal.valueOf(2)).compareTo(bookingOrderDTO.getProperty().getPrice()) == 1){
            List<PropertyDTO> availableProperties = propertyService.findAvailableProperties(bookingOrderDTO.getProperty().getCity(), bookingOrderDTO.getStartDate(), bookingOrderDTO.getEndDate());
            for(PropertyDTO propertyDTO : availableProperties){
                if(propertyDTO.get_id().equals(bookingOrderDTO.getProperty().get_id())){
                    BookingOrder bo = BookingOrder.of(bookingIdentifierFactory.nextBookingID(), bookingOrderDTO.getStartDate(), bookingOrderDTO.getEndDate(), null, userAssembler.toEntity(bookingOrderDTO.getUser()),propertyAssembler.toEntity(bookingOrderDTO.getProperty()), BookingOrderStatus.ACCEPTED);
                    bookingOrderRepository.save(bo);

                    return bookingOrderAssembler.toResource(bo);
                }
            }
        }
        //CREATE A BOOKING ORDER WITH STATUS REJECTED
        BookingOrder bo = BookingOrder.of(bookingIdentifierFactory.nextBookingID(), bookingOrderDTO.getStartDate(), bookingOrderDTO.getEndDate(), null, userAssembler.toEntity(bookingOrderDTO.getUser()),propertyAssembler.toEntity(bookingOrderDTO.getProperty()), BookingOrderStatus.REJECTED);
        bookingOrderRepository.save(bo);
        return bookingOrderAssembler.toResource(bo);
    }

    public BookingOrderDTO cancelBooking(String id) {
        BookingOrder bo = bookingOrderRepository.findOne(id);
        if(bo.getStartDate().isBefore(LocalDate.now().minusDays(7l))) {
            bo.updateStatus(BookingOrderStatus.CANCELLED);
            bookingOrderRepository.save(bo);
        } else {
            User user = bo.getUser();
            user.substractFromBalance(bo.getProperty().getPrice().divide(BigDecimal.valueOf(2)));
            userRepository.save(user);
        }
        return bookingOrderAssembler.toResource(bo);
    }

    public List<BookingOrderDTO> getAllBookings() {
        return bookingOrderAssembler.toResources(bookingOrderRepository.findAll());
    }

    public BookingOrderDTO addTransaction(String bookingOrder, String transactionURL) {
        BookingOrder bo = bookingOrderRepository.findOne(bookingOrder);
        bo.updateTransactionURL(transactionURL);
        bookingOrderRepository.save(bo);
        return bookingOrderAssembler.toResource(bo);
    }
}
