package com.example.booking.domain.model;

import com.example.property.domain.model.Property;
import com.example.user.domain.model.User;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * Created by lkpiel on 2/18/2017.
 */
@Entity
@Getter
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@EqualsAndHashCode
public class BookingOrder {
    @Id
    String id;
    LocalDate startDate;
    LocalDate endDate;
    String transaction;

    @ManyToOne
    User user;

    @ManyToOne
    Property property;
    @Enumerated(EnumType.STRING)
    BookingOrderStatus status;

    public void updateStatus(BookingOrderStatus status) {
        this.status = status;
    }
    public void updateTransactionURL(String url){
        this.transaction = url;
    }
}
