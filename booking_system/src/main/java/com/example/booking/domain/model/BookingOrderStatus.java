package com.example.booking.domain.model;

/**
 * Created by lkpiel on 6/9/2017.
 */
public enum BookingOrderStatus {
    PENDING, ACCEPTED, CANCELLED, CLOSED, REJECTED
}
