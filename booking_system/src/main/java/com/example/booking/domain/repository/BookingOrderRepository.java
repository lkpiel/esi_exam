package com.example.booking.domain.repository;

import com.example.booking.domain.model.BookingOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by lkpiel on 2/18/2017.
 */
@Repository
public interface BookingOrderRepository extends JpaRepository<BookingOrder, String> {

}
