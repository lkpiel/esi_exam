package com.example.booking.rest.controllers;

import com.example.booking.application.dto.BookingOrderDTO;
import com.example.booking.application.services.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

/**
 * Created by lkpiel on 5/24/2017.
 */
@CrossOrigin
@RestController
@RequestMapping("/api/bookings")
public class BookingRestController {
    @Autowired
    BookingService bookingService;

    @GetMapping("")
    public List<BookingOrderDTO> findAll() {
        return bookingService.getAllBookings();
    }

    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<BookingOrderDTO> makeBooking(@RequestBody BookingOrderDTO bookingOrderDTO) {
        System.out.println("Making booking with " + bookingOrderDTO.toString());
        BookingOrderDTO createdBookingDTO = bookingService.createBooking(bookingOrderDTO);
        if(createdBookingDTO == null){
            return new ResponseEntity<BookingOrderDTO>(null, null, HttpStatus.CONFLICT);

        }else {
            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(URI.create((createdBookingDTO.getId().getHref())));
            return new ResponseEntity<BookingOrderDTO>(createdBookingDTO, headers, HttpStatus.CREATED);
        }
    }
    @PostMapping("/{id}/cancel")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<BookingOrderDTO> cancelBooking(@PathVariable(value = "id") String id) {
        BookingOrderDTO updatedBookingDTO = bookingService.cancelBooking(id);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(URI.create((updatedBookingDTO.getId().getHref())));
        return new ResponseEntity<BookingOrderDTO>(updatedBookingDTO, headers, HttpStatus.OK);
    }
}
