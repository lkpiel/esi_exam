package com.example.booking.infrastructure;

import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * Created by Philosoraptor on 06/03/2017.
 */
@Service
public class BookingIdentifierFactory {
    public String nextBookingID() {
        return UUID.randomUUID().toString();
    }
}