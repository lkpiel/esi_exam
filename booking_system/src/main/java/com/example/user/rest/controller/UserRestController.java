package com.example.user.rest.controller;

import com.example.common.rest.dto.TransactionDTO;
import com.example.user.application.dto.UserDTO;
import com.example.user.application.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

/**
 * Created by lkpiel on 6/8/2017.
 */
@CrossOrigin
@RestController
@RequestMapping("/api/users")
public class UserRestController {
    @Autowired
    UserService userService;

    @PostMapping("/{id}/transactions")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<UserDTO> cancelBooking(@PathVariable(value = "id") String id,@RequestBody TransactionDTO transactionDTO) {
        UserDTO userDTO = userService.addTransaction(id, transactionDTO);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(URI.create((userDTO.getId().getHref())));
        return new ResponseEntity<UserDTO>(userDTO, headers, HttpStatus.OK);
    }
}
