package com.example.user.application.dto;

import com.example.booking.application.dto.BookingOrderDTO;
import com.example.user.domain.model.UserType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by lkpiel on 6/8/2017.
 */
@Data
@Getter
@EqualsAndHashCode
public class UserDTO extends com.example.common.rest.ResourceSupport {
    String _id;

    String username;
    String password;
    BigDecimal balance;
    String email;
    UserType type;
    List<BookingOrderDTO> bookings;

}
