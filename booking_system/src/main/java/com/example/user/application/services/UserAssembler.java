package com.example.user.application.services;

import com.example.user.application.dto.UserDTO;
import com.example.user.domain.model.User;
import com.example.user.rest.controller.UserRestController;
import com.example.booking.application.services.BookingOrderAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

/**
 * Created by lkpiel on 6/8/2017.
 */
@Service
public class UserAssembler extends ResourceAssemblerSupport<User, UserDTO> {
    @Autowired
    BookingOrderAssembler bookingOrderAssembler;

    public UserAssembler() {
        super(UserRestController.class, UserDTO.class);
    }

    @Override
    public UserDTO toResource(User user) {
        if (user != null) {
            UserDTO dto = createResourceWithId(user.getId(), user);
            dto.set_id(user.getId());
            dto.setEmail(user.getEmail());
            dto.setBalance(user.getBalance());
            dto.setPassword(user.getPassword());
            dto.setType(user.getType());
            dto.setBookings(bookingOrderAssembler.toResources(user.getBookings()));

            return dto;
        }
        return null;
    }
    public User toEntity(UserDTO userDTO) {
        if(userDTO != null) {
            User user = User.of(userDTO.get_id(), userDTO.getUsername(), userDTO.getPassword(), userDTO.getBalance(), userDTO.getEmail(), userDTO.getType(), bookingOrderAssembler.toEntities(userDTO.getBookings()));
            return user;
        }
        return null;
    }
}