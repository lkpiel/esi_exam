package com.example.user.application.services;

import com.example.booking.application.dto.BookingOrderDTO;
import com.example.booking.application.services.BookingService;
import com.example.common.rest.dto.TransactionDTO;
import com.example.user.application.dto.UserDTO;
import com.example.user.domain.model.User;
import com.example.user.domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by lkpiel on 6/8/2017.
 */
@Service
public class UserService {
    @Autowired
    BookingService bookingService;
    @Autowired
    UserRepository userRepository;
    @Autowired
    UserAssembler userAssembler;
    public UserDTO addTransaction(String userID, TransactionDTO transactionDTO) {
        BookingOrderDTO bo = bookingService.addTransaction(transactionDTO.getBookingOrder(), transactionDTO.getTransactionURL());
        User user = userRepository.findOne(userID);
        user.addToBalance(bo.getProperty().getPrice());
        userRepository.save(user);
        return userAssembler.toResource(user);
    }
}