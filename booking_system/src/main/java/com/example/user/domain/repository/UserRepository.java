package com.example.user.domain.repository;

import com.example.user.domain.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by lkpiel on 6/8/2017.
 */
@Repository
public interface UserRepository extends JpaRepository<User, String> {
}
