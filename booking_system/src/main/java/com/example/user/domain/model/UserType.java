package com.example.user.domain.model;

/**
 * Created by lkpiel on 6/9/2017.
 */
public enum UserType {
    HOST, TRAVELLER
}
