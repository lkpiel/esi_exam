package com.example.user.domain.model;

import com.example.booking.domain.model.BookingOrder;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by lkpiel on 6/8/2017.
 */
@Entity
@Getter
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@EqualsAndHashCode
public class User {
    @Id
    String id;
    String username;

    String password;
    @Column(precision = 8, scale = 2)
    BigDecimal balance;
    String email;

    @Enumerated(EnumType.STRING)
    UserType type;

    @OneToMany
    List<BookingOrder> bookings;

    public void substractFromBalance(BigDecimal substract) {
        this.balance = this.balance.subtract(substract);
    }
    public void addToBalance(BigDecimal addition) {
        this.balance = this.balance.add(addition);
    }
}
