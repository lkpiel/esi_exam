package com.example.property.application.services;

import com.example.property.application.dto.PropertyDTO;
import com.example.property.domain.model.Property;
import com.example.property.domain.repository.PropertyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by lkpiel on 6/8/2017.
 */
@Service
public class PropertyService {
    @Autowired
    PropertyRepository propertyRepository;
    @Autowired
    PropertyAssembler propertyAssembler;
    public List<PropertyDTO> findAvailableProperties(String city, LocalDate startDate, LocalDate endDate) {
        List<Property> properties = propertyRepository.findAvailableRooms(city, startDate, endDate);
        if(properties != null) {
            List<PropertyDTO> availableProperties = propertyAssembler.toResources(properties);
            return availableProperties;
        }
        return null;
    }
}
