package com.example.property.application.dto;

import com.example.booking.application.dto.BookingOrderDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by lkpiel on 6/8/2017.
 */
@Data
@Getter
@EqualsAndHashCode
public class PropertyDTO extends com.example.common.rest.ResourceSupport {
    String _id;
    BigDecimal price;
    String address;
    String city;
    String country;
    Integer floor;
    List<String> amenities;
    List<String> services;

    List<BookingOrderDTO> bookings;



}