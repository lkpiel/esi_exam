package com.example.property.application.services;

import com.example.booking.application.services.BookingOrderAssembler;
import com.example.property.application.dto.PropertyDTO;
import com.example.property.domain.model.Property;
import com.example.property.rest.controller.PropetryRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

/**
 * Created by lkpiel on 6/8/2017.
 */
@Service
public class PropertyAssembler extends ResourceAssemblerSupport<Property, PropertyDTO> {
    @Autowired
    BookingOrderAssembler bookingOrderAssembler;
    public PropertyAssembler() {
        super(PropetryRestController.class, PropertyDTO.class);
    }

    @Override
    public PropertyDTO toResource(Property propetry) {
        if (propetry != null) {
            PropertyDTO dto = createResourceWithId(propetry.getId(), propetry);
            dto.set_id(propetry.getId());
            dto.setAddress(propetry.getAddress());
            dto.setAmenities(propetry.getAmenities());
            dto.setBookings(bookingOrderAssembler.toResources(propetry.getBookings()));
            dto.setCity(propetry.getCity());
            dto.setCountry(propetry.getCountry());
            dto.setFloor(propetry.getFloor());
            dto.setPrice(propetry.getPrice());

            return dto;
        }
        return null;
    }

    public Property toEntity(PropertyDTO propertyDTO) {
        if (propertyDTO != null) {
            Property property = Property.of(propertyDTO.get_id(),propertyDTO.getPrice(),propertyDTO.getAddress(),propertyDTO.getCity(),propertyDTO.getCountry(),propertyDTO.getFloor(),propertyDTO.getAmenities(),propertyDTO.getServices(),bookingOrderAssembler.toEntities(propertyDTO.getBookings()));
            return property;
        }
        return null;
    }
}

