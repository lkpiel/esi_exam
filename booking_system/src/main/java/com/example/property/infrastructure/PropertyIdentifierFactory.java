package com.example.property.infrastructure;

import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * Created by lkpiel on 6/8/2017.
 */
@Service
public class PropertyIdentifierFactory {
    public String nextPropertyID() {
        return UUID.randomUUID().toString();
    }
}
