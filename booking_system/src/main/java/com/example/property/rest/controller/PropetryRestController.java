package com.example.property.rest.controller;

import com.example.property.application.dto.PropertyDTO;
import com.example.property.application.services.PropertyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Created by lkpiel on 6/8/2017.
 */
@CrossOrigin
@RestController
@RequestMapping("/api/properties")
public class PropetryRestController {
    @Autowired
    PropertyService propertyService;

    @GetMapping("")
    public List<PropertyDTO> findAvailableProperties(
            @RequestParam(name = "city", required = false) Optional<String> cityNameURI,
            @RequestParam(name = "startDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> startDateURI,
            @RequestParam(name = "endDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> endDateURI) {

        if (startDateURI.isPresent() && endDateURI.isPresent()) {
            if (endDateURI.get().isBefore(startDateURI.get()))
                throw new IllegalArgumentException("Something wrong with the requested period ('endDate' happens before 'startDate')");
        } else
            throw new IllegalArgumentException(
                    String.format("Wrong number of " +
                            "parameters: Start date='%s', End date='%s', city ='%s'", startDateURI.get(), endDateURI.get(), cityNameURI.get()));

        // get plants from rentIt side
        List<PropertyDTO> propertyDTOS = propertyService.findAvailableProperties(cityNameURI.get(), startDateURI.get(), endDateURI.get());

        return propertyDTOS;
    }
}
