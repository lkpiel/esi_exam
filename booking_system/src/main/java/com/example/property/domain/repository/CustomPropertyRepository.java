package com.example.property.domain.repository;

import com.example.property.domain.model.Property;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by lkpiel on 6/9/2017.
 */
public interface CustomPropertyRepository {
    List<Property> findAvailableRooms(String city, LocalDate startData, LocalDate endDate);
}
