package com.example.property.domain.repository;

import com.example.property.domain.model.Property;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by lkpiel on 6/8/2017.
 */
@Repository
public interface PropertyRepository extends JpaRepository<Property, String>, CustomPropertyRepository{
}
