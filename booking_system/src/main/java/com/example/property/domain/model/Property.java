package com.example.property.domain.model;

import com.example.booking.domain.model.BookingOrder;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by lkpiel on 6/8/2017.
 */
@Entity
@Getter
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@EqualsAndHashCode
public class Property {
    @Id
    String id;

    @Column(precision = 8, scale = 2)
    BigDecimal price;

    String address;
    String city;
    String country;
    Integer floor;
    @ElementCollection
    List<String> amenities;
    @ElementCollection
    List<String> services;

    @OneToMany
    List<BookingOrder> bookings;
}
