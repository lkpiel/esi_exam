package com.example.property.domain.repository;

import com.example.property.domain.model.Property;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.util.List;

/**
 * Created by lkpiel on 6/9/2017.
 */
public class PropertyRepositoryImpl implements CustomPropertyRepository{
    @Autowired
    EntityManager em;

    @Override
    public List<Property> findAvailableRooms(String city, LocalDate startDate, LocalDate endDate) {
        if(startDate.isAfter(LocalDate.now()) && (endDate.isAfter(LocalDate.now()) && endDate.isAfter(startDate))){
            List<Property> properties = em.createQuery("SELECT prop FROM Property prop WHERE prop.id NOT IN (SELECT bo.property.id FROM BookingOrder bo WHERE (?1 >= bo.startDate) AND (?2 <= bo.endDate) AND bo.status <> 'REJECTED')")
                    .setParameter(1, startDate)
                    .setParameter(2, endDate).getResultList();
            return  properties;
        }
        return null;
    }
}
