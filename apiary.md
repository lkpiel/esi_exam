FORMAT: 1A
HOST: http://codeitfor.me/

# ESI EXAM CodeItFor.me

CodeItFor.me is an online freelancer platform

## Projects Collection [api/projects]

### Retrieve Projects [GET]

+ Response 200 (application/json)

        [
            {
                "_id":"1",
                "name":"ESI Porject",
                "description":"Very simple Ruby project.",
                "status" : "OPEN",
                "_links" : {
                    "self" : {
                        "href" : "http://rentitfor.me/api/projects/1"
                    }
                },
                "questions" : [
                    {
                        "question" : "What is the front-end language of the project?",
                        "answers" : ["It should be Angular"],
                        "freelancer": "http://codeitfor.me/api/users/freelancer/1",
                        "_links" : {
                            "self" : {
                                "href" : "http://rentitfor.me/api/questions/1"
                            }
                        }
                    },
                    {
                        "question" : "Am I suitable?",
                        "answers" : ["Yes you are"],
                        "freelancer": "http://codeitfor.me/api/users/freelancer/2",
                        "_links" : {
                            "self" : {
                                "href" : "http://rentitfor.me/api/questions/2"
                            }
                        }
                    }
                ],
                "biddingPeriod": {
                    "startDate":{
                        "year":2017,
                        "month":7,
                        "day":1
                    },
                    "endDate":{
                        "year":2017,
                        "month":7,
                        "day":9
                    }
                },
                "proposals": [
                    {
                         "_id": "1"
                        "cost": 300.0,
                        "plan": "I believe I can do it one month",
                        "freelancer": "http://codeitfor.me/api/users/freelancer/1",
                        "portfolio" : "http://codeitfor.me/api/users/freelancer/1/profile",
                        "status" : "PENDING",
                        "_links" : {
                            "self" : {
                                "href" : "http://rentitfor.me/api/proposals/1"
                            }
                        }
                    }
                ]
            }
        ]

### Create a New Project [POST]

+ Request (application/json)

        {
            "name":"ESI Porject",
            "description":"Very simple Ruby project.",
            "biddingPeriod": {
                "startDate":{
                    "year":2017,
                    "month":7,
                    "day":1
                },
                "endDate":{
                    "year":2017,
                    "month":7,
                    "day":9
                }
            }
        }

+ Response 201 (application/json)

    + Headers
    
            Location: http://rentitfor.me/api/projects/1
        
    + Body
    
            {
                "statusCode":"CREATED",
                "headers":{
                    "Location":[
                        "http://rentitfor.me/api/projects/1"
                     ]
                },
                "body":{
                    "_id":"1",
                    "name":"ESI Porject",
                    "description":"Very simple Ruby project.",
                    "status" : "OPEN", 
                    "biddingPeriod": {
                        "startDate":{
                            "year":2017,
                            "month":7,
                            "day":1
                        },
                        "endDate":{
                            "year":2017,
                            "month":7,
                            "day":9
                        }
                    },
                    "questions" : [],
                    "proposals" : [],
                    "_links" : {
                        "self" : {
                            "href" : "http://rentitfor.me/api/projects/1"
                        }
                    }
               }
            }
            
## Project - Instance [/api/projects/{id}]
### Retrieve Project [GET]

+ Parameters
    + id: 100 (required, number) - Project ID in form of a long integer. Example: 11cc527e-1922-4888-a7c0-dc2ec8cd6f58

+ Response 200 (application/json)

 
        {
            "_id":"1",
            "name":"ESI Porject",
            "description":"Very simple Ruby project.",
            "status" : "OPEN",
            "_links" : {
                "self" : {
                    "href" : "http://rentitfor.me/api/projects/1"
                }
            },
            "questions" : [
                {
                    "_id": "1",
                    "question" : "What is the front-end language of the project?",
                    "answers" : ["It should be Angular"],
                    "freelancer": "http://codeitfor.me/api/users/freelancer/1",
                    "_links" : {
                        "self" : {
                            "href" : "http://rentitfor.me/api/questions/1"
                        }
                    }
                },
                {  
                    "_id": "2",
                    "question" : "Am I suitable?",
                    "answers" : ["Yes you are"],
                    "freelancer": "http://codeitfor.me/api/users/freelancer/2",
                    "_links" : {
                        "self" : {
                            "href" : "http://rentitfor.me/api/questions/2"
                        }
                    }
                }
            ],
            "biddingPeriod": {
                "startDate":{
                    "year":2017,
                    "month":7,
                    "day":1
                },
                "endDate":{
                    "year":2017,
                    "month":7,
                    "day":9
                }
            },
            "proposals": [
                {
                     "_id": "1"
                    "cost": 300.0,
                    "plan": "I believe I can do it one month",
                    "freelancer": "http://codeitfor.me/api/users/freelancer/1",
                    "portfolio" : "http://codeitfor.me/api/users/freelancer/1/profile",
                    "status" : "PENDING",
                    "_links" : {
                        "self" : {
                            "href" : "http://rentitfor.me/api/proposals/1"
                        }
                    }
                }
            ]
        }

### Update Project [PUT]
+ Parameters
    + id: 100 (required, number) - Project ID in form of a long integer. Example: 11cc527e-1922-4888-a7c0-dc2ec8cd6f58

+ Request (application/json)

        {
            "_id":"1",
            "name":"ESI Porject UPDATED",
            "description":"Very simple Ruby project."
        }
+ Response 200 (application/json)

    + Headers
    
            Location: http://rentitfor.me/api/projects/1
        
    + Body
    
            {
                "statusCode":"OK",
                "headers":{
                    "Location":[
                        "http://rentitfor.me/api/projects/1"
                     ]
                },
                "body":{
                    "_id":"1",
                    "name":"ESI Porject UPDATED",
                    "description":"Very simple Ruby project.",
                    "status" : "OPEN", 
                    "_links" : {
                        "self" : {
                            "href" : "http://rentitfor.me/api/projects/1"
                        }
                    },
                    "questions" : [
                        {
                            "_id": "1",
                            "question" : "What is the front-end language of the project?",
                            "answers" : ["It should be Angular"],
                            "freelancer": "http://codeitfor.me/api/users/freelancer/1",
                            "_links" : {
                                "self" : {
                                    "href" : "http://rentitfor.me/api/questions/1"
                                }
                            }
                        },
                        {
                            "_id": "2",
                            "question" : "Am I suitable?",
                            "answers" : ["Yes you are"],
                            "freelancer": "http://codeitfor.me/api/users/freelancer/2",
                            "_links" : {
                                "self" : {
                                    "href" : "http://rentitfor.me/api/questions/2"
                                }
                            }
                        }
                    ],
                    "biddingPeriod": {
                        "startDate":{
                            "year":2017,
                            "month":7,
                            "day":1
                        },
                        "endDate":{
                            "year":2017,
                            "month":7,
                            "day":9
                        }
                    },
                    "proposals": [
                        {
                            "cost": 300.0,
                            "plan": "I believe I can do it one month",
                            "freelancer": "http://codeitfor.me/api/users/freelancer/1",
                            "portfolio" : "http://codeitfor.me/api/users/freelancer/1/profile",
                            "status" : "PENDING",
                            "_links" : {
                                "self" : {
                                    "href" : "http://rentitfor.me/api/proposals/1"
                                }
                            }
                        }
                    ]
                }
            }
            
## Project - Award [/api/projects/{id}/award]
### Award project to a freelancer [POST]
+ Parameters
    + id: 100 (required, number) - Project ID in form of a long integer. Example: 11cc527e-1922-4888-a7c0-dc2ec8cd6f58

+ Request (application/json)

        {
            "proposal_id": "1",
        }

+ Response 200 (application/json)

    + Headers
    
            Location: http://rentitfor.me/api/projects/1
        
    + Body
    
            {
                "statusCode":"OK",
                "headers":{
                    "Location":[
                        "http://rentitfor.me/api/projects/1"
                     ]
                },
                "body":{
                    "_id":"1",
                    "name":"ESI Porject UPDATED",
                    "description":"Very simple Ruby project.",
                    "status" : "AWARDED", 
                    "_links" : {
                        "self" : {
                            "href" : "http://rentitfor.me/api/projects/1"
                        }
                    },
                    "questions" : [
                        {
                            "_id": "1",
                            "question" : "What is the front-end language of the project?",
                            "answers" : ["It should be Angular"],
                            "freelancer": "http://codeitfor.me/api/users/freelancer/1",
                            "_links" : {
                                "self" : {
                                    "href" : "http://rentitfor.me/api/questions/1"
                                }
                            }
                        },
                        {
                            "_id": "2",
                            "question" : "Am I suitable?",
                            "answers" : ["Yes you are"],
                            "freelancer": "http://codeitfor.me/api/users/freelancer/2",
                            "_links" : {
                                "self" : {
                                    "href" : "http://rentitfor.me/api/questions/2"
                                }
                            }
                        }
                    ],
                    "biddingPeriod": {
                        "startDate":{
                            "year":2017,
                            "month":7,
                            "day":1
                        },
                        "endDate":{
                            "year":2017,
                            "month":7,
                            "day":9
                        }
                    },
                    "proposals": [
                        {
                            "_id": "1"
                            "cost": 300.0,
                            "plan": "I believe I can do it one month",
                            "freelancer": "http://codeitfor.me/api/users/freelancer/1",
                            "portfolio" : "http://codeitfor.me/api/users/freelancer/1/profile",
                            "status" : "AWARDED",
                            "_links" : {
                                "self" : {
                                    "href" : "http://rentitfor.me/api/proposals/1"
                                }
                            }
                        }
                    ]
                }
            }
                    
           
## Project - Withdraw [/api/projects/{id}/withdraw]
### Withdraw a project [POST]
+ Parameters
    + id: 100 (required, number) - Project ID in form of a long integer. Example: 11cc527e-1922-4888-a7c0-dc2ec8cd6f58

+ Response 200 (application/json)

    + Headers
    
            Location: http://rentitfor.me/api/projects/1
        
    + Body
    
            {
                "statusCode":"OK",
                "headers":{
                    "Location":[
                        "http://rentitfor.me/api/projects/1"
                     ]
                },
                "body":{
                    "_id":"1",
                    "name":"ESI Porject UPDATED",
                    "description":"Very simple Ruby project.",
                    "status" : "WITHDRAWN",
                    "_links" : {
                        "self" : {
                            "href" : "http://rentitfor.me/api/projects/1"
                        }
                    },
                    "questions" : [
                        {
                            "_id": "1",
                            "question" : "What is the front-end language of the project?",
                            "answers" : ["It should be Angular"],
                            "freelancer": "http://codeitfor.me/api/users/freelancer/1",
                            "_links" : {
                                "self" : {
                                    "href" : "http://rentitfor.me/api/questions/1"
                                }
                            }
                        },
                        {
                            "_id": "2",
                            "question" : "Am I suitable?",
                            "answers" : ["Yes you are"],
                            "freelancer": "http://codeitfor.me/api/users/freelancer/2",
                            "_links" : {
                                "self" : {
                                    "href" : "http://rentitfor.me/api/questions/2"
                                }
                            }
                        }
                    ],
                    "biddingPeriod": {
                        "startDate":{
                            "year":2017,
                            "month":7,
                            "day":1
                        },
                        "endDate":{
                            "year":2017,
                            "month":7,
                            "day":9
                        }
                    },
                    "proposals": [
                        {
                            "_id": "1"
                            "cost": 300.0,
                            "plan": "I believe I can do it one month",
                            "freelancer": "http://codeitfor.me/api/users/freelancer/1",
                            "portfolio" : "http://codeitfor.me/api/users/freelancer/1/profile",
                            "status" : "AWARDED",
                            "_links" : {
                                "self" : {
                                    "href" : "http://rentitfor.me/api/proposals/1"
                                }
                            }
                        }
                    ]
                }
            }
                    
## Project - Pay [/api/projects/{id}/pay]
### Mark project as paid [POST]
+ Parameters
    + id: 100 (required, number) - Project ID in form of a long integer. Example: 11cc527e-1922-4888-a7c0-dc2ec8cd6f58

+ Response 200 (application/json)

    + Headers
    
            Location: http://rentitfor.me/api/projects/1
        
    + Body
    
            {
                "statusCode":"OK",
                "headers":{
                    "Location":[
                        "http://rentitfor.me/api/projects/1"
                     ]
                },
                "body":{
                    "_id":"1",
                    "name":"ESI Porject UPDATED",
                    "description":"Very simple Ruby project.",
                    "status" : "PAID",
                    "_links" : {
                        "self" : {
                            "href" : "http://rentitfor.me/api/projects/1"
                        }
                    },
                    "questions" : [
                        {
                            "_id": "1",
                            "question" : "What is the front-end language of the project?",
                            "answers" : ["It should be Angular"],
                            "freelancer": "http://codeitfor.me/api/users/freelancer/1",
                            "_links" : {
                                "self" : {
                                    "href" : "http://rentitfor.me/api/questions/1"
                                }
                            }
                        },
                        {
                            "_id": "2",
                            "question" : "Am I suitable?",
                            "answers" : ["Yes you are"],
                            "freelancer": "http://codeitfor.me/api/users/freelancer/2",
                            "_links" : {
                                "self" : {
                                    "href" : "http://rentitfor.me/api/questions/2"
                                }
                            }
                        }
                    ],
                    "biddingPeriod": {
                        "startDate":{
                            "year":2017,
                            "month":7,
                            "day":1
                        },
                        "endDate":{
                            "year":2017,
                            "month":7,
                            "day":9
                        }
                    },
                    "proposals": [
                        {
                            "_id": "1"
                            "cost": 300.0,
                            "plan": "I believe I can do it one month",
                            "freelancer": "http://codeitfor.me/api/users/freelancer/1",
                            "portfolio" : "http://codeitfor.me/api/users/freelancer/1/profile",
                            "status" : "AWARDED",
                            "_links" : {
                                "self" : {
                                    "href" : "http://rentitfor.me/api/proposals/1"
                                }
                            }
                        }
                    ]
                }
            }
  
## Project - Close [/api/projects/{id}/close]
### Mark project as closed [DELETE]
+ Parameters
    + id: 100 (required, number) - Project ID in form of a long integer. Example: 11cc527e-1922-4888-a7c0-dc2ec8cd6f58

+ Response 200 (application/json)

    + Headers
    
            Location: http://rentitfor.me/api/projects/1
        
    + Body
    
            {
                "statusCode":"OK",
                "headers":{
                    "Location":[
                        "http://rentitfor.me/api/projects/1"
                     ]
                },
                "body":{
                    "_id":"1",
                    "name":"ESI Porject UPDATED",
                    "description":"Very simple Ruby project.",
                    "status" : "CLOSED",
                    "_links" : {
                        "self" : {
                            "href" : "http://rentitfor.me/api/projects/1"
                        }
                    },
                    "questions" : [
                        {
                            "_id": "1",
                            "question" : "What is the front-end language of the project?",
                            "answers" : ["It should be Angular"],
                            "freelancer": "http://codeitfor.me/api/users/freelancer/1",
                            "_links" : {
                                "self" : {
                                    "href" : "http://rentitfor.me/api/questions/1"
                                }
                            }
                        },
                        {
                            "_id": "2",
                            "question" : "Am I suitable?",
                            "answers" : ["Yes you are"],
                            "freelancer": "http://codeitfor.me/api/users/freelancer/2",
                            "_links" : {
                                "self" : {
                                    "href" : "http://rentitfor.me/api/questions/2"
                                }
                            }
                        }
                    ],
                    "biddingPeriod": {
                        "startDate":{
                            "year":2017,
                            "month":7,
                            "day":1
                        },
                        "endDate":{
                            "year":2017,
                            "month":7,
                            "day":9
                        }
                    },
                    "proposals": [
                        {
                            "_id": "1"
                            "cost": 300.0,
                            "plan": "I believe I can do it one month",
                            "freelancer": "http://codeitfor.me/api/users/freelancer/1",
                            "portfolio" : "http://codeitfor.me/api/users/freelancer/1/profile",
                            "status" : "AWARDED",
                            "_links" : {
                                "self" : {
                                    "href" : "http://rentitfor.me/api/proposals/1"
                                }
                            }
                        }
                    ]
                }
            }
                                 
           
## Questions - Collection [/api/questions/]
### Create a New Question [POST]

+ Request (application/json)

        {
            "question" : "Am I suitable?",
            "project" : "http://rentitfor.me/api/projects/1"
        }

+ Response 201 (application/json)

    + Headers
    
            Location: http://rentitfor.me/api/questions/1
        
    + Body
    
            {
                "_id": "1",
                "question" : "What is the front-end language of the project?",
                "answers" : ["It should be Angular"],
                "freelancer": "http://codeitfor.me/api/users/freelancer/1",
                "_links" : {
                    "self" : {
                        "href" : "http://rentitfor.me/api/questions/1"
                    }
                }
            }

### Retrieve Questions [GET]

+ Response 200 (application/json)

        [
            {
                "_id": "1",
                "question" : "What is the front-end language of the project?",
                "answers" : ["It should be Angular"],
                "freelancer": "http://codeitfor.me/api/users/freelancer/1",
                "_links" : {
                    "self" : {
                        "href" : "http://rentitfor.me/api/questions/1"
                    }
                }
            },
            {
                "_id": "2",
                "question" : "Am I suitable?",
                "answers" : ["Yes you are"],
                "freelancer": "http://codeitfor.me/api/users/freelancer/2",
                "_links" : {
                    "self" : {
                        "href" : "http://rentitfor.me/api/questions/2"
                    }
                }
            }
        ]
        
## Question - Instance [/api/questions/{id}]
### Retrieve Question [GET]

+ Parameters
    + id: 100 (required, number) - Question ID in form of a long integer. Example: 11cc527e-1922-4888-a7c0-dc2ec8cd6f58

+ Response 200 (application/json)

        {
            "_id": "1",
            "question" : "What is the front-end language of the project?",
            "answers" : ["It should be Angular"],
            "freelancer": "http://codeitfor.me/api/users/freelancer/1",
            "_links" : {
                "self" : {
                    "href" : "http://rentitfor.me/api/questions/1"
                }
            }
        }

## Profile - Collection [/api/profiles/]
### Create a New Profile [POST]

+ Request (application/json)

        {
            "name" : "Leo Kristopher Piel",
            "email" : "lkpiel@masdi.com",
            "education" : "Bachelor's degree in Business Information Technology",
            "githubUser" : "",
            "bitbucketUser" : "lkpiel",
            "skype" : "lkpiel",
            "country" : "Estonia",
            "skills" :["Ruby", "Spring"],
            "links" : {
                "website", "mindplace.eu",
                "linkedIn","http://linked.in"
            },
            "freelancer" : "http://rentitfor.me/api/freelancer/1"
        }

+ Response 201 (application/json)

    + Headers
    
            Location: http://rentitfor.me/api/profiles/1
        
    + Body
    
            {
                "_id": "1",
                "name" : "Leo Kristopher Piel",
                "email" : "lkpiel@masdi.com",
                "education" : "Bachelor's degree in Business Information Technology",
                "githubUser" : "",
                "bitbucketUser" : "lkpiel",
                "skype" : "lkpiel",
                "country" : "Estonia",
                "skills" :["Ruby", "Spring"],
                "links" : [["website", "mindplace.eu"], ["linkedIn","http://linked.in"]]
                "_links" : {
                    "self" : {
                        "href" : "http://rentitfor.me/api/profiles/1"
                    }
                }
            }

### Retrieve Profiles [GET]

+ Response 200 (application/json)

        [
            {
                "_id": "1",
                "name" : "Leo Kristopher Piel",
                "email" : "lkpiel@masdi.com",
                "education" : "Bachelor's degree in Business Information Technology",
                "githubUser" : "",
                "bitbucketUser" : "lkpiel",
                "skype" : "lkpiel",
                "country" : "Estonia",
                "skills" :["Ruby", "Spring"],
                "links" : [["website", "mindplace.eu"], ["linkedIn","http://linked.in"]]
                "_links" : {
                    "self" : {
                        "href" : "http://rentitfor.me/api/profiles/1"
                    }
                }
            }
        ]
        
## Profile - Instance [/api/profiles/{id}]
### Retrieve Profile [GET]

+ Parameters
    + id: 100 (required, number) - Profile ID in form of a long integer. Example: 11cc527e-1922-4888-a7c0-dc2ec8cd6f58

+ Response 200 (application/json)

        {
            "_id": "1",
            "name" : "Leo Kristopher Piel",
            "email" : "lkpiel@masdi.com",
            "education" : "Bachelor's degree in Business Information Technology",
            "githubUser" : "",
            "bitbucketUser" : "lkpiel",
            "skype" : "lkpiel",
            "country" : "Estonia",
            "skills" :["Ruby", "Spring"],
            "links" : [["website", "mindplace.eu"], ["linkedIn","http://linked.in"]]
            "_links" : {
                "self" : {
                    "href" : "http://rentitfor.me/api/profiles/1"
                }
            }
        }